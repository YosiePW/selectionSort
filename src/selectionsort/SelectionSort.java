package selectionsort;
import java.util.Arrays;
public class SelectionSort {

    void pertama(int array[]) {
        int n = array.length;
        // untuk banyaknya swap
        System.out.println("Proses iterasi : ");
        
        for (int i = 0; i < n; i++) {
          System.out.println("Iterasi ke-" +(i+1)+ " : ");  
          System.out.println(Arrays.toString(array));
            // untuk menemukan nilai terkecil
            int minNilai = i;
            for (int j = i + 1; j < n; j++) {
                if (array[j] < array[minNilai]) {
                    minNilai = j;
                }
            }
            // swap angka terbesar dengan angka pertama
            int temp = array[minNilai];
            array[minNilai] = array[i];
            array[i] = temp;
        }
    }
    // untuk menampilkan angka yang telah diproses
    void kedua(int array[]) {
        int n = array.length;
        for (int i = 0; i < n; ++i) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        System.out.println("Yosieka Putri Wibawa/ X RPL6 / 40 \n");
        SelectionSort obj = new SelectionSort();
        int array[] = {8, 3, 6, 1, 4, 2, 9, 10, 15, 11};
        System.out.println(" Angka Sebelum di proses :\n " + Arrays.toString(array) + "\n");
        obj.pertama(array);
        System.out.println("\n Angka Setelah di proses :\n " );
        obj.kedua(array);
    }
}